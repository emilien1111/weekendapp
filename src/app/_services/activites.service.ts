import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Activite } from '../_models';

@Injectable()
export class ActivitesService {
    constructor(private http: HttpClient) { }

    getAll() {
        return ACTIVITES;
        //return this.http.get<Activite[]>(`${environment.apiUrl}/activites`);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/activites/` + id);
    }

    update(activite: Activite) {
        return this.http.put(`${environment.apiUrl}/activites/` + activite.id, activite);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/activites/` + id);
    }
}

const ACTIVITES = [
  { id: 1, name: 'Aviron', inscrit: false },
  { id: 2, name: 'Voile', inscrit: false },
  { id: 3, name: 'Surf', inscrit: true },
  { id: 4, name: 'Kayak', inscrit: false }
];
