import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Region, Departement, Ville } from '../_models';

@Injectable()
export class LieuxService {
    constructor(private http: HttpClient) { }

    getAllRegions() {
        return REGIONS;
        //return this.http.get<Activite[]>(`${environment.apiUrl}/activites`);
    }

    getAllDepartement() {
        return DEPARTEMENTS;
        //return this.http.get<Activite[]>(`${environment.apiUrl}/activites`);
    }

    getAllVille() {
        return VILLES;
        //return this.http.get<Activite[]>(`${environment.apiUrl}/activites`);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/lieux/` + id);
    }

    update(region: Region) {
        return this.http.put(`${environment.apiUrl}/lieux/` + region.id, region);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/lieux/` + id);
    }
}

const REGIONS = [
  { id: 1, name: 'Bretagne', departements: ['Finistere', 'Morbihan'], inscrit: true },
  { id: 2, name: 'Pays De La Loire', departements: ['Vendee', "Deux-Sevres"], inscrit: false }
];
const DEPARTEMENTS = [
  { id: 3, name: 'Finistere', villes: ['Quimper'], inscrit: true },
  { id: 4, name: 'Morbihan', villes: ['Vannes'], inscrit: false },
  { id: 5, name: 'Vendee', villes: ['La Roche Sur Yon'], inscrit: true },
  { id: 6, name: 'Deux-Sevres', villes: ['Niort'], inscrit: false }
];
const VILLES = [
  { id: 7, name: 'Quimper', inscrit: true },
  { id: 8, name: 'Vannes', inscrit: false },
  { id: 9, name: 'La Roche Sur Yon', inscrit: true },
  { id: 10, name: 'Niort', inscrit: false }
];
