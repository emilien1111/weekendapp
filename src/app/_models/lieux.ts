export class Region {
    id: number;
    name: string;
    departements: string[];
    inscrit: boolean;
}

export class Departement {
    id: number;
    name: string;
    villes: string[];
    inscrit: boolean;
}

export class Ville {
    id: number;
    name: string;
    inscrit: boolean;
}
