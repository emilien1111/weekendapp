import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '../_models';
import { UserService } from '../_services';
import { Router } from '@angular/router';
import { ActivitesComponent } from '../components/activites/activites';

@Component({templateUrl: 'home.component.html'})
export class HomeComponent {
    currentUser: User;

    constructor(private userService: UserService, private router: Router) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    areYouSure() {
      if(confirm("You are about to delete your account, all data will be lost.")){
        this.deleteUser();
        this.router.navigate(['/login']);
      }
    }

    deleteUser() {
        this.userService.delete(this.currentUser.id).pipe(first()).subscribe(() => {});
    }
}
