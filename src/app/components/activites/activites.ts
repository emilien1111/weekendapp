import { Component, OnInit } from '@angular/core';

import { Activite } from '../../_models';
import { ActivitesService } from '../../_services';

@Component({selector: 'app-activites', templateUrl: './activites.html'})
export class ActivitesComponent implements OnInit {

  activitesInscrit: Array<Activite>;
  activites: Array<Activite>;


  constructor(private activitesService: ActivitesService) {
    //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }


  ngOnInit() {
    this.loadAllActivites();
  }

  private loadAllActivites() {
    /*
    this.activitesService.getAll().pipe(first()).subscribe(activites => {
      this.activites = activites;
    });
    */
    this.activitesInscrit = new Array(0);
    var that = this;
    this.activites = this.activitesService.getAll();
    this.activites.map(function(activite){
      if (activite.inscrit){
        that.activitesInscrit.push(activite);
        that.activites = that.activites.filter(function(activiteBis){
          if(activite != activiteBis){
            return activiteBis;
          } else {
            return null;
          }
        });
      }
      return;
    });
      /*
      if (activite.inscrit){
        this.activitesInscrit.add(activite);
        this.activites.remove(activite);
      }
      */
  }

  private inscrire(id : number){

  }

}
