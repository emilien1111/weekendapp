# projet TAA getionnaire de weekend en bords de mer

Fait par Le Mikael et Petit Emilien.

Le but du projet est de développer une application permettant à un utilisateur de recevoir un mail tous les mercredi lui indiquant une liste de lieux (ville, département, région) où les conditions météorologiques sont idéales le week-end pour pratiquer des sports nautiques.

Ce dépôt contient la partie front end réalisée en angular, la partie front-end ne communique pas avec la partie back-end car nous n'avons pas réussi à faire le lien en utilisant REST.

Voici le dossier front-end de notre projet TAA, nous avons suivi comme base de ce projet du tutoriel suivant :

Angular 6 - User Registration and Login Example with Angular CLI

To see a demo and further details go to http://jasonwatmore.com/post/2018/05/16/angular-6-user-registration-and-login-example-tutorial

Pour créer la base de connection d'un utilisateur.

#Utilisation de l'application

L'application est un site web où l'utilisateur doit créer un compte avant de poursuivre. Après la création du compte, il devra se connecter pour avoir accés aux fonctionnalités de l'application.
L'utilisateur va ensuite pouvoir choisir les activités qu'il souhaite faire le week-end. La personne devra aussi déterminer où il est possible pour lui de passer son week-end, que ce soit des régions, des départements ou des villes.

Quand toutes ces informations sont connues par l'application, celle-ci devrait envoyer un mail automatiquement tous les mardi soir indiquant les lieux et activités idéaux pour le week-end. Cependant, comme le front end et le back-end ne communiquent pas il nous est impossible de finaliser cette partie. L'utilisateur peut modifier ses informations quand il le souhaite et supprimer son profil.

#Lancement de l'application

Pour faire fonctionner la partie front-end, il faut cloner le repository git et lancer le serveur avec une commande "ng serve" pour ensuite accéder à l'application à l'adresse http://localhost:4200/ depuis un navigateur. A ce jour la partie front-end est auto-suffisante.

#Ce qu'il reste à faire

Comme dit en introduction, lors de la réalisation de la partie front-end, nous avons commencé par nous inspirer du tutoriel de Jason Watmore ce qui nous as permis d'avoir rapidement une base pour la connexion et la création de compte, mais après un travail de design et avoir commencé à ajouter des components nous nous sommes rendus compte que c'était une erreur car nous avons eu beaucoup de mal par la suite à ajouter de nouvelles fonctionnalitées du fait de ne pas maîtriser entièrement l'organisation du site.

Au final il nous faut encore finaliser la création des components relatifs aux activités et lieux et surtout faire un lien avec le back-end pour pouvoir se débarasser du back-end simulé que nous avons utilisé jusque là.

